Copyright 2007-2011 Alexander Mikhailian <mikhailian@mova.org>

Module
------
moderation_log: Moderation Log

Description
-----------
This module logs the modifications of nodes and comments done by non-authors
(that is, anyone with the "administer content" or "administer comments"
privileges) and displays the statistics on these modifications in a block as
well as a detailed view of the modifications on separate pages.

Every modification can be given a reason. Reasons come from a vocabulary that
can be defined in the configuration page.

Configuration
-------------
Go to /admin/settings/moderation_log to configure the vocabulary that will provide
moderation reasons for the node edit and node delete pages.


Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.


